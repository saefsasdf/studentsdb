
// MyStudentsDBDlg.h : ���� ���������
//
#pragma once
#include "afxwin.h"


#include <string.h>
#include <iostream>
using namespace std;

// ���������� ���� CMyStudentsDBDlg
class CMyStudentsDBDlg : public CDialogEx
{
// ��������
public:
	CMyStudentsDBDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_MYSTUDENTSDB_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CListBox m_list1;
	CButton ok_btn;
//	afx_msg void OnLbnSelchangeList1();
	afx_msg void OnDblclkList1();
	afx_msg void OnLbnSelchangeList1();
	void CMyStudentsDBDlg::MyShowMes(string title, string text);
	void CMyStudentsDBDlg::LoadListBox();
};
