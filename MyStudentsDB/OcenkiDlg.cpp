// OcenkiDlg.cpp: ���� ����������
//

#include "stdafx.h"
#include "MyStudentsDB.h"
#include "OcenkiDlg.h"
#include "afxdialogex.h"


// ���������� ���� COcenkiDlg

IMPLEMENT_DYNAMIC(COcenkiDlg, CDialogEx)

COcenkiDlg::COcenkiDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(COcenkiDlg::IDD, pParent)
	, ePredmet(_T(""))
	, eOcenka(_T(""))
	, eSession(_T(""))
{

}

COcenkiDlg::~COcenkiDlg()
{
}

void COcenkiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, ePredmet);
	DDX_Text(pDX, IDC_EDIT2, eOcenka);
	DDX_Text(pDX, IDC_EDIT3, eSession);
}


BEGIN_MESSAGE_MAP(COcenkiDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &COcenkiDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// ����������� ��������� COcenkiDlg


void COcenkiDlg::OnBnClickedOk()
{
	// TODO: �������� ���� ��� ����������� �����������
	CDialogEx::OnOK();
}
