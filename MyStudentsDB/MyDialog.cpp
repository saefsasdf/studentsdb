// MyDialog.cpp: ���� ����������
//

#include "stdafx.h"
#include "MyStudentsDB.h"
#include "MyDialog.h"
#include "OcenkiDlg.h"

#include "afxdialogex.h"
#include "afxwin.h"
#include "odbcinst.h"
#include "afxdb.h"

// ���������� ���� MyDialog

IMPLEMENT_DYNAMIC(MyDialog, CDialogEx)

MyDialog::MyDialog(CWnd* pParent /*=NULL*/)
: CDialogEx(MyDialog::IDD, pParent)
, eFamilia(_T(""))
, eName(_T(""))
, eOtchestvo(_T(""))
, DTP(COleDateTime::GetCurrentTime())
, eFacultet(_T(""))
, eKafedra(_T(""))
, eZachetka(_T(""))
, eDateEntry(_T(""))
{
}

MyDialog::~MyDialog()
{
}

void MyDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, eFamilia);
	DDX_Text(pDX, IDC_EDIT2, eName);
	DDX_Text(pDX, IDC_EDIT3, eOtchestvo);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, DTP);
	DDX_Text(pDX, IDC_EDIT4, eFacultet);
	DDX_Text(pDX, IDC_EDIT5, eKafedra);
	DDX_Text(pDX, IDC_EDIT6, eZachetka);
	DDX_Text(pDX, IDC_EDIT7, eDateEntry);
	DDX_Control(pDX, IDC_BUTTON1, btnSave);
	DDX_Control(pDX, IDC_BUTTON2, btnExit);
	DDX_Control(pDX, IDC_LIST2, m_List);
}


BEGIN_MESSAGE_MAP(MyDialog, CDialogEx)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDOK, &MyDialog::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &MyDialog::OnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &MyDialog::OnBnClickedButton2)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST2, &MyDialog::OnLvnItemchangedList2)
 
	ON_BN_CLICKED(IDC_BUTTON3, &MyDialog::OnBtnAdd)
	ON_BN_CLICKED(IDC_BUTTON4, &MyDialog::OnBtnEdit)
	ON_BN_CLICKED(IDC_BUTTON5, &MyDialog::OnBtnDelete)
	ON_BN_CLICKED(IDC_BUTTON6, &MyDialog::OnBnClickedButton6)
END_MESSAGE_MAP()


// ����������� ��������� MyDialog


int MyDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  �������� ������������������ ��� ��������

	return 0;
}


BOOL MyDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  �������� �������������� �������������

	CDatabase database;
	CString SqlString;
 
	CString sYear, sYear_entery, sFamile, sFaculty, sDeportament, sGroup, sNum_Stud, sNum_Zachet, sFamily, sName, sOtchestvo;
	int sDay, sMonth, sYears;

	CString sDriver = L"MICROSOFT ACCESS DRIVER (*.mdb)";
	CString sDsn;
	CString sFile = L"Test.mdb";
	// You must change above path if it's different
	int iRec = 0;
	int i = 0;

	sDsn.Format(L"ODBC;DRIVER={%s};DSN='';DBQ=%s", sDriver, sFile);

	
	TRY
	{
		// Open the database
		database.Open(NULL, false, false, sDsn);
		
		// Allocate the recordset
		CRecordset recset(&database);

		//CString t;
		//t.Format(_T("%d"), (number+1));

		// Build the SQL statement
		SqlString = L"SELECT * FROM Students ORDER BY ��� ASC";
		
		// Execute the query
		recset.Open(CRecordset::forwardOnly, SqlString, CRecordset::readOnly); 
		// populate Grids
		// Loop through each record
		while (!recset.IsEOF()) 
		{
			if (i == (number)) break;
			// goto next record
			recset.MoveNext();
			i++;
			
		}
		
		// ���
		//recset.GetFieldValue(L"�������", sFamile);
		recset.GetFieldValue(L"�������", this->sFamile);
		recset.GetFieldValue(L"���", sName);
		recset.GetFieldValue(L"��������", sOtchestvo);

		recset.GetFieldValue(L"���_�����������", sYear_entery);
		recset.GetFieldValue(L"���������", sFaculty);
		recset.GetFieldValue(L"�������", sDeportament);
		recset.GetFieldValue(L"�_��������_������", sNum_Zachet);
	 
		recset.GetFieldValue(L"����_��������", sYear);

		sDay = dayy(sYear);
		sMonth = monthh(sYear);
		sYears = yearr(sYear);

		//AfxMessageBox(L"Database break" + this->sFamile);

		// Close the database
		database.Close();
	}
		CATCH(CDBException, e)
	{
		// If a database exception occured, show error msg
		AfxMessageBox(L"Database error: " + e->m_strError);
	}
	END_CATCH;


	//MessageBox(_T("OnInitDialog() ")+this->sFamile, _T("OnInitDialog"), MB_OK | MB_ICONQUESTION);

	UpdateData();
	DTP.SetDate(sYears, sMonth, sDay);

	eFamilia = this->sFamile;
	eName = sName;
	eOtchestvo = sOtchestvo;
	eFacultet = sFaculty;
	eKafedra = sDeportament;
	eZachetka = sNum_Zachet;
	eDateEntry = sYear_entery;		 

	UpdateData(FALSE);

	m_List.InsertColumn(0, _T("�������"), LVCFMT_LEFT, 100);
	m_List.InsertColumn(1, _T("������"), LVCFMT_LEFT, 70);
	m_List.InsertColumn(2, _T("� ������"), LVCFMT_LEFT, 70);
	
    FillList();
	UpdateData(false);
	return TRUE;  // return TRUE unless you set the focus to a control
	// ����������: �������� ������� OCX ������ ���������� �������� FALSE
}


void MyDialog::OnBnClickedOk()
{
	// TODO: �������� ���� ��� ����������� �����������
 
}

void MyDialog::setFields(){
 
}

int MyDialog::dayy(CString str)
{
	int day = 1;
	int tchk = 0;
	int i_tchk = 0;
	char *str_out;

	for (int i = 0; i < 10; i++)
	{
		if (str[i] == '-')
		{
			tchk++;
		}

		if (tchk == 2)
		{
			i_tchk = i + 1;
			break;
		}
	}

	str_out = new char[2];
	int j = 0;

	for (int i = i_tchk; i < 10; i++)
	{
		str_out[j] = str[i];
		j++;
	}

	day = atoi(str_out);
	return day;
}

int MyDialog::monthh(CString str)
{
	int day = 1;
	int tchk = 0;
	int i_tchk1 = 0;
	int i_tchk2 = 0;
	char *str_out;

	for (int i = 0; i < 10; i++)
	{
		if (str[i] == '-')
		{
			tchk++;
		}

		if (tchk == 1)
		{
			i_tchk1 = i - 1;
		}

		if (tchk == 2)
		{
			i_tchk2 = i;
			break;
		}
	}

	str_out = new char[2];
	int j = 0;
	for (int i = i_tchk1; i < i_tchk2; i++)
	{
		str_out[j] = str[i];
		j++;
	}

	day = atoi(str_out);
	return day;
}

int MyDialog::yearr(CString str)
{
	int day = 1;
	int tchk = 0;
	int i_tchk = 0;
	char *str_out;

	for (int i = 0; i < str.GetLength(); i++)
	{
		if (str[i] == '-')
		{
			tchk++;
		}

		if (tchk == 1)
		{
			i_tchk = i;
			break;
		}
	}

	str_out = new char[3];
	int j = 0;
	for (int i = 0; i <i_tchk; i++)
	{
		str_out[j] = str[i];
		j++;
	}

	day = atoi(str_out);
	return day;
}

void MyDialog::OnClickedButton1()
{
	// TODO: �������� ���� ��� ����������� �����������
	CString str;
	//ListBox1.GetText(ListBox1.GetCurSel(), str);
	//str = string_vibor(str);
	CDatabase database;
	CString SqlString;
	CString sYear, sYear_entery, sFaculty, sDeportament, sGroup, sNum_Stud, sNum_Zachet, sFamily, sName, sOtchestvo;
	CString sDriver = L"MICROSOFT ACCESS DRIVER (*.mdb)";
	CString sDsn;
	CString sFile = L"Test.mdb";
	// You must change above path if it's different
	int iRec = 0;
	int i = 0;

	sDsn.Format(L"ODBC;DRIVER={%s};DSN='';DBQ=%s", sDriver, sFile);
	TRY
	{
		// Open the database
		database.Open(NULL, false, false, sDsn);
		CString Date_Year;
		UpdateData();
		Date_Year.Format(L"%i.%i.%i", DTP.GetDay(), DTP.GetMonth(), DTP.GetYear());

		char buffer[33];
		itoa((number+1), buffer, 10);
		database.ExecuteSQL(L"UPDATE Students Set �������='" + eFamilia + "', ���='" + eName + "', ��������='" + eOtchestvo + "', ����_��������='" + Date_Year + "', ���_�����������='" + eDateEntry + "', ���������='" + eFacultet + "', �������='" + eKafedra + "', �_��������_������='" + eZachetka + "' where ���=" + buffer);
		database.Close();
 
		EndDialog(1);
	}
		CATCH(CDBException, e)
	{
		// If a database exception occured, show error msg
		AfxMessageBox(L"Database error: " + e->m_strError);
	}
	END_CATCH;
}


void MyDialog::OnBnClickedButton2()
{
	// TODO: �������� ���� ��� ����������� �����������
	EndDialog(0);
}


void MyDialog::OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: �������� ���� ��� ����������� �����������
	*pResult = 0;
}



void MyDialog::OnBtnAdd()
{
	// TODO: �������� ���� ��� ����������� �����������
	COcenkiDlg odlg;
	if (IDOK == odlg.DoModal()){
		//MessageBox(odlg.eOcenka, _T("����� ��������"));

		CDatabase database;
		CString SqlString;
		CString sCatID, sCategory;
		CString sDriver = L"MICROSOFT ACCESS DRIVER (*.mdb)";
		CString sDsn;
		CString sFile = L"Test.mdb";
		// You must change above path if it's different
		int iRec = 0;

		sDsn.Format(L"ODBC;DRIVER={%s};DSN='';DBQ=%s", sDriver, sFile);
		//MessageBox(_T("OnBtnAdd() ") + this->sFamile, _T("OnBtnAdd"), MB_OK | MB_ICONQUESTION);
		try
		{
			database.Open(NULL, false, false, sDsn);

			CRecordset recset(&database);
			UpdateData();
			CString str_date;
			database.ExecuteSQL(L"INSERT INTO Test1 (�������, �������, ������, ������) VALUES ('" + this->eFamilia + "','" + odlg.ePredmet + "','" + odlg.eOcenka + "', '" + odlg.eSession + "')");
			//UpdateData(false);
			//MessageBox(L"������!");
			database.Close();
			FillList();
			UpdateData(false);
		}
		catch (CDBException e)
		{
			MessageBox(e.m_strError, L"������ ���������� ������ �� ��������.");
		}
	}
}


void MyDialog::OnBtnEdit()
{
	// TODO: �������� ���� ��� ����������� �����������
	//MessageBox(_T("OnBtnEdit() ") + this->sFamile, _T("OnBtnEdit"), MB_OK | MB_ICONQUESTION);
}


void MyDialog::OnBtnDelete()
{
	int nIndex;
	if (m_List.GetSelectedCount() == 0){
		MessageBox(L"�������� ������� ��� ��������.", L"����������", MB_OK | MB_ICONINFORMATION);
		return;
	}
	POSITION pos = m_List.GetFirstSelectedItemPosition();
	if (pos != NULL){
		nIndex = m_List.GetNextSelectedItem(pos);
	}

	CString tmp = m_List.GetItemText(nIndex, 0);
	//MessageBox(tmp, L"����������", MB_OK | MB_ICONINFORMATION);

	//return;

	// Delete ocenka
	// TODO: Add your control notification handler code here
	CDatabase database;
	CString SqlString;
	CString sDriver = L"MICROSOFT ACCESS DRIVER (*.mdb)";
	CString sDsn;
	CString sFile = L"Test.mdb";

	sDsn.Format(L"ODBC;DRIVER={%s};DSN='';DBQ=%s", sDriver, sFile);
	TRY
	{
		// Open the database
		database.Open(NULL, false, false, sDsn);
		// Allocate the recordset
		CRecordset recset(&database);
		// Build the SQL statement
		database.ExecuteSQL(L"DELETE FROM Test1 WHERE ������� = '" + this->eFamilia + L"' AND ������� = '" + tmp + L"';");
		database.Close();
	}
		CATCH(CDBException, e)
	{
		// If a database exception occured, show error msg
		AfxMessageBox(L"Database error: " + e->m_strError);
	}
	END_CATCH;
}

void MyDialog::FillList()
{
	CDatabase database;
	CString SqlString;
	CString sCatID, sName, sOtchestvo;
	CString sDriver = L"MICROSOFT ACCESS DRIVER (*.mdb)";
	CString sDsn;
	CString sFile = L"Test.mdb";
	// You must change above path if it's different
	int iRec = 0;

	sDsn.Format(L"ODBC;DRIVER={%s};DSN='';DBQ=%s", sDriver, sFile);

	int nIndex = 0;

	TRY
	{ 
		m_List.DeleteAllItems();
		// Open the database
		database.Open(NULL, false, false, sDsn);

		// Allocate the recordset
		CRecordset recset(&database);

		CString t;
		t.Format(_T("%d"), (number + 1));

		// Build the SQL statement
		SqlString = L"SELECT * FROM Test1 WHERE ������� = '" + this->eFamilia + L"';";

		// Execute the query
		recset.Open(CRecordset::forwardOnly, SqlString, CRecordset::readOnly);

		// populate Grids

		int i = 0;
		while (!recset.IsEOF())
		{
			recset.GetFieldValue(L"�������", sFamile);
			recset.GetFieldValue(L"������", sName);
			recset.GetFieldValue(L"������", sOtchestvo);

			AddData(m_List, i, 0, sFamile);
			AddData(m_List, i, 1, sName);
			AddData(m_List, i, 2, sOtchestvo);
			// goto next record
			recset.MoveNext();
			i++; nIndex++;
		}
		// Close the database
		database.Close();
	}
		CATCH(CDBException, e)
	{
		// If a database exception occured, show error msg
		AfxMessageBox(L"Database error: " + e->m_strError);
	}
	END_CATCH;

}

void MyDialog::AddData(CListCtrl &ctrl, int row, int col, CString str)
{
	LVITEM lv;
	lv.iItem = row;
	lv.iSubItem = col;
	lv.pszText = (LPWSTR)(LPCWSTR)str; // (LPWSTR)str;
	lv.mask = LVIF_TEXT;
	if (col == 0)
		ctrl.InsertItem(&lv);
	else
		ctrl.SetItem(&lv);
}


void MyDialog::OnBnClickedButton6()
{
	// Delete user

	CDatabase database;
	CString SqlString;
	CString sDriver = L"MICROSOFT ACCESS DRIVER (*.mdb)";
	CString sDsn;
	CString sFile = L"Test.mdb";

	sDsn.Format(L"ODBC;DRIVER={%s};DSN='';DBQ=%s", sDriver, sFile);
	TRY
	{
		// Open the database
		database.Open(NULL, false, false, sDsn);
		// Allocate the recordset
		CRecordset recset(&database);
		// Build the SQL statement
		database.ExecuteSQL(L"DELETE FROM Students WHERE ������� = '" + this->eFamilia + L"';");
		database.Close();
	}
	CATCH(CDBException, e)
	{
		// If a database exception occured, show error msg
		AfxMessageBox(L"Database error: " + e->m_strError);
	}
	END_CATCH;
}
