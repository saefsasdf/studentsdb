#pragma once
#include "afxwin.h"

#include "ATLComTime.h"
#include "afxcmn.h"

// ���������� ���� MyDialog

class MyDialog : public CDialogEx
{
	DECLARE_DYNAMIC(MyDialog)

public:
	
	MyDialog(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~MyDialog();

	MyDialog(CString _userID);

// ������ ����������� ����
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	void setFields();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	CString eFamilia;
	int number;
	CString UserFIO;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	CString eName;
	CString eOtchestvo;
	CString sFamile;

	int dayy(CString str);
	int monthh(CString str);
	int yearr(CString str);

	COleDateTime DTP;
	CString eFacultet;
	CString eKafedra;
	CString eZachetka;
	CString eDateEntry;
	CButton btnSave;
	CButton btnExit;
	afx_msg void OnClickedButton1();
	afx_msg void OnBnClickedButton2();
//	CListCtrl m_List;
	CListCtrl m_List;
	afx_msg void OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult);
 
	afx_msg void OnBtnAdd();
	afx_msg void OnBtnEdit();
	afx_msg void OnBtnDelete();

	void MyDialog::FillList();
	void MyDialog::AddData(CListCtrl &ctrl, int row, int col, CString str);
	afx_msg void OnBnClickedButton6();
};
