﻿
// MyStudentsDBDlg.cpp : файл реализации
//

#include "stdafx.h"
#include "MyStudentsDB.h"
#include "MyStudentsDBDlg.h"
//#include "MAWrapper.h"
#include "afxdialogex.h"
#include "afxwin.h"
#include "odbcinst.h"
#include "afxdb.h"
 
#include "MyDialog.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
public:
	CButton ok_btn;
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, ok_btn);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// диалоговое окно CMyStudentsDBDlg



CMyStudentsDBDlg::CMyStudentsDBDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMyStudentsDBDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMyStudentsDBDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list1);
	DDX_Control(pDX, IDOK, ok_btn);
}

BEGIN_MESSAGE_MAP(CMyStudentsDBDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CMyStudentsDBDlg::OnBnClickedOk)
//	ON_LBN_SELCHANGE(IDC_LIST1, &CMyStudentsDBDlg::OnLbnSelchangeList1)
	ON_LBN_DBLCLK(IDC_LIST1, &CMyStudentsDBDlg::OnDblclkList1)
	ON_LBN_SELCHANGE(IDC_LIST1, &CMyStudentsDBDlg::OnLbnSelchangeList1)
END_MESSAGE_MAP()


// обработчики сообщений CMyStudentsDBDlg

BOOL CMyStudentsDBDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	LoadListBox();

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CMyStudentsDBDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CMyStudentsDBDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CMyStudentsDBDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMyStudentsDBDlg::OnBnClickedOk()
{ 

}

void CMyStudentsDBDlg::OnDblclkList1()
{
	// TODO: добавьте свой код уведомлений

	CString str;
	m_list1.GetText(m_list1.GetCurSel(), str);
	//MessageBox(NULL, str, NULL); 
	//MyShowMes();
}


void CMyStudentsDBDlg::OnLbnSelchangeList1()
{
	int tmp;
	CString str;
	m_list1.GetText(m_list1.GetCurSel(), str);

	int number = m_list1.GetCurSel();
	//CString t;
	//t.Format(_T("%d"), number);
	//MessageBox(t, _T("Msg title"), MB_OK | MB_ICONQUESTION);

	MyDialog  md = new MyDialog();
	md.number = number;
	md.setFields();
	tmp = md.DoModal();
 
 if (tmp == 1) {
	 MessageBox(_T("Данные успешно обновлены."), _T("Сохранение"), MB_OK | MB_ICONQUESTION);
	 LoadListBox();
 }
}

void CMyStudentsDBDlg::MyShowMes(std::string title, std::string text)
{
 
	MessageBox(_T("Text"), _T("Msg title"), MB_OK | MB_ICONQUESTION);

}

void CMyStudentsDBDlg::LoadListBox()
{
	// ListBox1.ResetContent();
	CDatabase database;
	CString SqlString;
	CString sFamile, sName, sOtchestvo;
	CString sDriver = L"MICROSOFT ACCESS DRIVER (*.mdb)";
	CString sDsn;
	CString sFile = L"Test.mdb";
	// You must change above path if it's different
	int iRec = 0;
	int i = 0;

	sDsn.Format(L"ODBC;DRIVER={%s};DSN='';DBQ=%s", sDriver, sFile);

	TRY
	{
		// Open the database
		database.Open(NULL, false, false, sDsn);

		// Allocate the recordset
		CRecordset recset(&database);

		// Build the SQL statement
		SqlString = L"SELECT * FROM Students ORDER BY Код ASC";

		// Execute the query
		recset.Open(CRecordset::forwardOnly, SqlString, CRecordset::readOnly);
		m_list1.ResetContent();

		// populate Grids
		// Loop through each record
		while (!recset.IsEOF())
		{

			// ФИО
			recset.GetFieldValue(L"Фамилия", sFamile);
			recset.GetFieldValue(L"Имя", sName);
			recset.GetFieldValue(L"Отчество", sOtchestvo);

			// Insert values into the list control
			m_list1.AddString(sFamile + L" " + sName + L" " + sOtchestvo);

			// goto next record
			recset.MoveNext();
			i++;
		}
		i = 0;
		// Close the database
		database.Close();
	}
		CATCH(CDBException, e)
	{
		// If a database exception occured, show error msg
		AfxMessageBox(L"Database error: " + e->m_strError);
	}
	END_CATCH;
}


